"""Op5 Monitor hosts to Ansible inventory

This script creates an Ansible inventory from Op5 Monitor generated json files.

Written by farid@joubbi.se 2019-05-16

"""

import datetime
import glob
import json
import re


def init_inventory(inventory_file = "inventory"):
    """Opens a new Ansible inventory file or overwrites an old one with a timestamp.
    """

    file_out = open(inventory_file,'w')
    file_out.write("#  --== File generated with opfem2ansible.py " + datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S") + " ==--" + '\n')
    file_out.close()


def file_read(op5_hosts_filename):
    """Reads a json file generated from Op5 Monitor and returns a list of hosts.
    """

    with open(op5_hosts_filename, 'r') as f:
        op5hosts_dict = json.load(f)

    hosts_lst = []
    for devices in op5hosts_dict:
        hosts_lst.append(devices['name'])

    return hosts_lst


def inventory_write(op5hosts_file, group, inventory_file = "inventory"):
    """Writes hosts and group names to a file in Ansible inventory format.
    """

    file_out = open(inventory_file, 'a')
    file_out.write('\n' + "["+group+"]" + '\n')
    for item in file_read(op5hosts_file):
        file_out.write(item + '\n')
    file_out.close()


def get_groupnames_from_files(json_file_directory = "hosts_json"):
    """"Outputs filenames and names of groups derived from the filename.
    """

    json_files = glob.glob(json_file_directory + '/*.json')
    pattern = re.compile(json_file_directory + '/op5_hosts_(\S+).json')
    #pattern = re.compile(r'hosts_json/op5_hosts_(\S+).json')
    matched_files = filter(pattern.search, json_files)

    group_names = []
    if matched_files:
        for match in matched_files:
            group_names.append(pattern.search(match).group(1))
    return matched_files, group_names


def main():
    init_inventory()

   # inventory_write('access', 'hosts_json/op5_hosts_access.json')

    matched_files, group_names = get_groupnames_from_files()

    for group_name, file_name in zip(group_names, matched_files):
	inventory_write(file_name, group_name)


if __name__ == '__main__':
    main()

