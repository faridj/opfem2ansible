# opfem2ansible

This is a script that creates an inventory for [__Ansible__](https://www.ansible.com/) from [__Op5 Monitor__](https://www.op5.com/) using the Op5 API.

The script reads json files fetched from Op5 Monitor and writes an inventory for Ansible based on the information from the json files.
One group per json file is created. The name of the group is based on the name of the json file.

## Example

A file named hosts_json/op5_hosts_access.json creates a group named access, which might look like this:
```
#  --== File generated with opfem2ansible.py 2019-05-16 15:49:29 ==--

[access]
access-switch01.example.com
access-switch02.example.com
access-switch03.example.com
access-switch04.example.com
access-switch05.example.com
access-switch06.example.com
```

An easy way of creating a json file from Op5 Monitor is by doing this:
```
# [hosts] groups >= "Access"
curl -u 'username$Default:password' "https://op5-server.example.com/api/filter/query?query=%5Bhosts%5D%20groups%20%3E%3D%20%22Access%22&columns=name&limit=1000" --insecure -H "content-type: application/json" > hosts_json/op5_hosts_access.json
```



___

Licensed under the [__Apache License Version 2.0__](https://www.apache.org/licenses/LICENSE-2.0)

Written by __farid@joubbi.se__

http://www.joubbi.se/monitoring.html

